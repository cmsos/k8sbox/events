# Preparation

on lxplus8.cern.ch

```
export CLUSTER_NAME="<cluster name>"

export KUBECONFIG=$HOME/cern/openstack/$CLUSTER_NAME/config

kubectl get nodes

kubectl label nodes <node>-# events.node.role=true
```
Detailed informaton on [ creating a K8s cluster ](https://gitlab.cern.ch/cmsos/kube/-/wikis/Creating%20a%20Kubernetes%20cluster%20in%20OpenStack) on IT services

# Helm commands

```
helm repo add events-devel https://gitlab.cern.ch/api/v4/projects/177090/packages/helm/devel

helm repo update

helm search repo --devel

helm uninstall events
```

### Installation on general purpose cluster

```
helm install events events-devel/cmsos-events-helm --devel`
```

### Installation in CMS cluster

```
helm install --set zone.name=development --set zone.imagepullsecrets=false --set zone.createnamespace=false events events-devel/cmsos-events-helm --devel
```

# kubectl commands

kubectl get pods -n events

kubectl get services -n events -o wide

kubectl exec -ti <podid> -n events -- bash

## Running on daq3val K8s cluster

### Prepare cluster

```
kubectl config use-context kubernetes-admin@kubernetes
kubectl get nodes
kubectl label nodes d3vrubu-c2e34-06-01 events.node.role=true
kubectl label nodes d3vrubu-c2e34-08-01 events.node.role=true
kubectl label nodes d3vrubu-c2e34-10-01 events.node.role=true
```

### install events

```
helm repo add events-devel https://gitlab.cern.ch/api/v4/projects/177090/packages/helm/devel
helm install events events-devel/cmsos-events-helm --devel
helm install events events-devel/cmsos-events-helm --devel --set zone.application.source.replicas=3
```

### Bash directly into pod and CURL 
```
kubectl exec -ti source-??????  -n events -- bash
export K8S_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
cat >> event.json
curl -k -v -X POST -H "Authorization: Bearer $K8S_TOKEN" -H "Content-Type: application/json" https://10.254.0.1:443/api/v1/namespaces/events/events -d@event.json
```

### Watching for events
```
kubectl get events -n events  --watch
kubectl get events --field-selector type=Normal -n events  --watch  -o=jsonpath='{.metadata.name} {.eventTime} {.involvedObject.name} {.reportingInstance} {.action} {.reason}{"\n"}'
kubectl get events -n events  --watch  -o=jsonpath='{.metadata.name} {.eventTime} {.involvedObject.name} {.reportingInstance} {.action} {.reason}{"\n"}' --selector type=events.fsm.state
```

### Adding core project as a submodule of k8s/events
```
$ cd k8sbox/events
$ git submodule add https://gitlab.cern.ch/cmsos/core
$ cd core
$ git checkout <branch, tag or commit>
$ cd k8sbox/events
$ git add .gitsubmodule core
$ git commit -m "Added submodule"
$ git push
```

### Modifying files inside submodule directory
```
$ cd k8sbox/events/core
$ git add <file name>
$ git commit -m "message"
$ git push
$ cd k8sbox/events
$ git add core
$ git commit -m "Updated submodule"
$ git push
```
